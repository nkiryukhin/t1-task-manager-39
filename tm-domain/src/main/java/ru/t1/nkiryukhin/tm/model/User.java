package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Role;

import java.util.Objects;

@Getter
@Setter
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

}
