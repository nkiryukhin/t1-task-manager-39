package ru.t1.nkiryukhin.tm.api.service;

public interface IDomainService {

    void dropDatabase(String passphrase);

    void createDatabase();

}